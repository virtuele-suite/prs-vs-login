package com.sys.virtuele.login.services;

import com.sys.virtuele.login.models.UserAuthResponse;
import com.sys.virtuele.login.models.UserCredDto;

public interface LoginService {

	public UserAuthResponse authenticateAndAuthorizeUser(UserCredDto userCredDto);
	
}
