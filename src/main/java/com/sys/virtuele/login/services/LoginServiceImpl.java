package com.sys.virtuele.login.services;

import org.springframework.stereotype.Service;

import com.sys.virtuele.login.models.UserAuthResponse;
import com.sys.virtuele.login.models.UserCredDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LoginServiceImpl extends LoginResource implements LoginService{
	
	
	@Override
	public UserAuthResponse authenticateAndAuthorizeUser(UserCredDto userCredDto) {
		
		UserAuthResponse userAuthResponse =  authenticateUser(userCredDto);
		
		return userAuthResponse;
	}


}
