package com.sys.virtuele.login.services;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.sys.virtuele.login.models.UserAuthResponse;
import com.sys.virtuele.login.models.UserCredDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoginResource {
	// final String companyBaseUri = "http://localhost:6011/api/v1/company/";
	final String userBaseUri = "http://localhost:6012/api/v1/user/";
	// final String projectBaseUri = "http://localhost:6013/api/v1/project/";
	
	@Autowired
	private RestTemplate restTemplate;

	protected UserAuthResponse authenticateUser(UserCredDto userCredDto) {
		
		String path = "auth";
		String url = userBaseUri + path;
		
		log.info(">> User Authentication URL : " + url);
		//UserAuthResponse userAuthResponse = restTemplate.getForObject(url, UserAuthResponse.class);

		//return userAuthResponse;

	
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(userCredDto,headers);

		ResponseEntity<UserAuthResponse> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,new ParameterizedTypeReference<UserAuthResponse>() {});

		return response.getBody();
		
		
	}

		
}
