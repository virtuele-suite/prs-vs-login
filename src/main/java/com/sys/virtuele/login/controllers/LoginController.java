package com.sys.virtuele.login.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.sys.virtuele.login.models.UserAuthResponse;
import com.sys.virtuele.login.models.UserCredDto;
import com.sys.virtuele.login.services.LoginService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author pnc
 *
 */
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1/login")
@RestController
@CrossOrigin
@ControllerAdvice
public class LoginController {

	@Autowired
	private LoginService loginService;

	@PostMapping("/auth")
	public ResponseEntity<UserAuthResponse> getRfiDetailsAtCompanyLevel(@RequestBody UserCredDto userCredDto) {
		try {
			log.info("<< User is being Authenticated !");

			UserAuthResponse userAuthResponse = loginService.authenticateAndAuthorizeUser(userCredDto);

			HttpHeaders headers = new HttpHeaders();

			headers.add("Location", "/api/v1/login/auth/" + userAuthResponse.getUserAuthToken());
			return new ResponseEntity<UserAuthResponse>(userAuthResponse, HttpStatus.OK);
		} catch (HttpClientErrorException h4xx) {
			return new ResponseEntity<UserAuthResponse>(h4xx.getResponseHeaders(), h4xx.getStatusCode());
		} catch (HttpServerErrorException h5xx) {
			return new ResponseEntity<UserAuthResponse>(h5xx.getResponseHeaders(), h5xx.getStatusCode());
		}
	}
}
