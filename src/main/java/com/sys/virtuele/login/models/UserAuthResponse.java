package com.sys.virtuele.login.models;

import java.util.Optional;

import com.pro.virtuele.common.models.User;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserAuthResponse {
	private Boolean userAuthStatus;
	private String userAuthToken;
	private String userAuthDetails;
	private String userMessage;
	private Optional<User> user;

}
