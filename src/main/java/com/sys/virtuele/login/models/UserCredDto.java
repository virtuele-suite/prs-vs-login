package com.sys.virtuele.login.models;

import lombok.Data;

@Data
public class UserCredDto {
	private String userName;
	private String password;
	private String accessToken;
}
